module.exports = {
  preset: 'ts-jest',
  bail: true,
  testEnvironment: 'node',
  testMatch: [
    '**/__tests__/**/*.spec.ts?(x)',
  ]
};