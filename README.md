# Spot Metrics Backend Challend

## Objective

A simple REST API to manage kitchen inventory.

## Requirements

| Application    | Version    | Download                                                               |
| -------------- | :--------- | :-------------------------------------------------------               |
| Node           | >= 12.x.x  | [Download](https://nodejs.org/en/download)                            |
| Docker         | >= 20.10.0 | [Download](https://docs.docker.com/engine/install)                    |
| Docker Compose | >= 1.27.2  | [Download](https://docs.docker.com/compose/install/#install-compose)   |

## Setting Up Environment

## Setting environment variables

Use the `.env.example` file and define the environment variables according to your choice;

## Starting the application with docker-compose

In your application directory, run:

```bash
docker-compose up -d
```

## Documentation

After starting the application, the documentation will be available on the route `http://localhost:<your-port>/api/v1`

## Terminating the application

To shut down the application, enter your application directory and run:

```bash
docker-compose down
```

### Enjoy
