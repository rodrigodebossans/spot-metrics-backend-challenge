import { ObjectID } from 'mongodb';
import { EntityManager, EntityRepository } from 'typeorm';
import { NonExistentObjectException } from '../../exception/non-existent-object.exception';
import { UninformedObjectException } from '../../exception/uninformed-object.exception';
import { Ingredient } from '../../model/ingredient/ingredient';

@EntityRepository()
export class IngredientRepository {
  constructor(private manager: EntityManager) {}

  public async getIngredientById(id: ObjectID): Promise<Ingredient> {
    if (id) {
      const ingredientFound = await this.manager.findOne(Ingredient, id);

      if (ingredientFound) {
        return ingredientFound;
      } else throw new NonExistentObjectException();
    } else throw new UninformedObjectException();
  }
}
