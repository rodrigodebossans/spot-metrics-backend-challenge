export enum UnityEnum {
  MILLIGRAM = 'milligram',
  GRASS = 'grass',
  KILOGRAM = 'kilogram',
}
