export enum HttpStatusCodeEmum {
  OK = 200,
  CREATED = 201,
  BAD_REQUEST = 400,
}