import app from './app';

export class Server {
  constructor() {}

  public static start(): void {
    const port = parseInt(process.env.SERVICE_CORE_PORT) || 32751;
    app.express.listen(port, '0.0.0.0', async () => {
      await app.createsDatabaseConnection();
      console.info(`The core module is running on port ${port}`);
    });
  }
}

Server.start();