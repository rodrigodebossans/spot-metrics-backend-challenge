import { GeneralException } from './general.exception';

export class ExistingObjectException extends GeneralException {
  constructor(cause?: any) {
    super(
      'ExistingObjectException',
      'The object you are looking for already exists in our database and cannot be registered with the given name',
      cause
    );
  }
}
