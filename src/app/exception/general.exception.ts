export class GeneralException implements Error {
  public name: string;
  public message: string;
  public cause: any;

  constructor(name: string, message: string, cause?: any) {
    this.name = name;
    this.message = message;

    if (cause) this.cause = cause;
  }

  toString(): string {
    return `Error: ${this.name}, Message: ${this.message}`;
  }
}
