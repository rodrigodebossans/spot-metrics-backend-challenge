import { Router } from 'express';

import IngredientController from './ingredient/ingredient.controller';

class Controllers {
  public readonly routes = Router();

  constructor() {
    this.loadRoutes();
  }

  public getRoutes(): any {
    return this.routes;
  }

  private loadRoutes(): void {
    this.routes.post('/ingredient', IngredientController.createIntegredient);
    this.routes.get('/ingredient', IngredientController.listAll);
    this.routes.put('/ingredient/:id', IngredientController.editIngredient);
    this.routes.delete(
      '/ingredient/:id',
      IngredientController.deleteIntegredient
    );
  }
}

export default new Controllers().routes;
