import { Request, Response } from 'express';
import { ObjectID } from 'mongodb';
import { HttpStatusCodeEmum } from '../../enum/http-status-code/http-status-code.enum';
import { Ingredient } from '../../model/ingredient/ingredient';
import ingredientService from '../../service/ingredient/ingredient.service';

class IngredientController {
  public async createIntegredient(
    req: Request,
    res: Response
  ): Promise<Response<Ingredient>> {
    try {
      return res
        .status(HttpStatusCodeEmum.CREATED)
        .send(
          await ingredientService.createIngredient(
            Ingredient.fromObj(req?.body)
          )
        );
    } catch (error) {
      return res.status(HttpStatusCodeEmum.BAD_REQUEST).send({ error });
    }
  }

  public async listAll(
    req: Request,
    res: Response
  ): Promise<Response<Ingredient[]>> {
    try {
      return res
        .status(HttpStatusCodeEmum.OK)
        .send(Ingredient.fromArray(await ingredientService.listAll()));
    } catch (error) {
      return res.status(HttpStatusCodeEmum.BAD_REQUEST).send({ error });
    }
  }

  public async editIngredient(
    req: Request,
    res: Response
  ): Promise<Response<Ingredient>> {
    try {
      return res
        .status(HttpStatusCodeEmum.OK)
        .send(
          Ingredient.fromObj(
            await ingredientService.editIngredient(
              Ingredient.fromObj(req?.body),
              req.params?.id
            )
          )
        );
    } catch (error) {
      return res.status(HttpStatusCodeEmum.BAD_REQUEST).send({ error });
    }
  }

  public async deleteIntegredient(
    req: Request,
    res: Response
  ): Promise<Response<Ingredient>> {
    try {
      return res
        .status(HttpStatusCodeEmum.OK)
        .send(
          await ingredientService.deleteIngredient(new ObjectID(req.params?.id))
        );
    } catch (error) {
      return res.status(HttpStatusCodeEmum.BAD_REQUEST).send({ error });
    }
  }
}

export default new IngredientController();
