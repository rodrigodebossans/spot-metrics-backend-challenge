import {
  IsEnum,
  IsNotEmpty,
  MaxLength,
  validate,
  ValidationError
} from 'class-validator';
import { ObjectID } from 'mongodb';
import {
  Column,
  CreateDateColumn,
  Entity,
  ObjectIdColumn,
  UpdateDateColumn
} from 'typeorm';
import { UnityEnum } from '../../enum/unity/unity.enum';
import { UninformedObjectException } from '../../exception/uninformed-object.exception';

@Entity()
export class Ingredient {
  constructor() {}

  @ObjectIdColumn()
  public id?: ObjectID;

  @MaxLength(143, {
    message:
      'The $property property is too long. Maximal length is $constraint1 characters, but actual is $value',
  })
  @IsNotEmpty({
    message: 'The $property property is required',
  })
  @Column()
  public name: string;

  @IsNotEmpty({
    message: 'The $property property is required',
  })
  @Column()
  public weight: number;

  @IsEnum(UnityEnum, {
    message:
      'The $property can only receive milligram values | grass | kilogram',
  })
  @IsNotEmpty({
    message: 'The $property property is required',
  })
  @Column()
  public unity: UnityEnum;

  @CreateDateColumn()
  public creationDate?: Date;

  @UpdateDateColumn()
  public modifiedDate?: Date;

  public static fromObj(obj: any): Ingredient {
    if (obj) {
      const ingredient = new Ingredient();

      if (obj?.id) ingredient.id = obj?.id;

      ingredient.name = obj?.name;
      ingredient.weight = obj?.weight;
      ingredient.unity = obj?.unity;

      if (obj?.creationDate) ingredient.creationDate = obj?.creationDate;

      if (obj?.modifiedDate) ingredient.modifiedDate = obj?.modifiedDate;

      return ingredient;
    }
  }

  public static fromArray(arr: any[]): Ingredient[] {
    return arr.map((obj: any) => Ingredient.fromObj(obj));
  }

  public static async validate(ingredient: Ingredient): Promise<void> {
    const validationErrors: ValidationError[] = await validate(ingredient);
    if (validationErrors.length)
      throw new UninformedObjectException(validationErrors);
  }
}
