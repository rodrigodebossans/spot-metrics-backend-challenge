import bodyParser from 'body-parser';
import cors from 'cors';
import dotenv from 'dotenv';
import express from 'express';
import 'reflect-metadata';
import swaggerUi from 'swagger-ui-express';
import { createConnection } from 'typeorm';
import yaml from 'yamljs';
import controllers from './controller';

class App {
  public express: express.Application;
  private readonly HOME_PATH = '/api/v1/';

  constructor() {
    dotenv.config();
    this.express = express();
    this.loadMiddlewares();
    this.loadCors();
    this.loadRoutes();
    this.loadDocumentation();
  }

  private loadMiddlewares(): void {
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
  }

  private loadCors(): void {
    const corsOptions: cors.CorsOptions = {
      allowedHeaders: [
        'Origin',
        'X-Requested-With',
        'Content-Type',
        'Accept',
        'X-Access-Token',
      ],
      credentials: true,
      methods: 'GET,POST,PUT,PATCH,DELETE',
      preflightContinue: false,
      origin: '*',
      optionsSuccessStatus: 200,
    };

    this.express.use(cors(corsOptions));
  }

  public async createsDatabaseConnection(): Promise<void> {
    try {
      await createConnection();
      console.info('Database connection successfully established');
    } catch (error) {
      console.error(error);
    }
  }

  private loadRoutes(): void {
    this.express.use(this.HOME_PATH, controllers);
  }

  private loadDocumentation(): void {
    const yamlFilePath = `${__dirname}/docs/swagger/swagger.yaml`;
    const swaggerSpecs = yaml.load(yamlFilePath);
    this.express.use(
      this.HOME_PATH,
      swaggerUi.serve,
      swaggerUi.setup(swaggerSpecs)
    );
  }
}

export default new App();
