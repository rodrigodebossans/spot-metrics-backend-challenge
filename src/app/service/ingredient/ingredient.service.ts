import { ObjectID } from 'mongodb';
import { getMongoManager, getCustomRepository } from 'typeorm';
import { ExistingObjectException } from '../../exception/existing-object.exception';
import { Ingredient } from '../../model/ingredient/ingredient';
import { IngredientRepository } from '../../repository/ingredient/ingredient.repository';

class IngredientService {
  constructor() {}

  public async createIngredient(ingredient: Ingredient): Promise<Ingredient> {
    await Ingredient.validate(ingredient);

    const repository = getMongoManager().getRepository(Ingredient);
    const { name } = ingredient;

    const predicate = { name };

    const ingredientAlreadyExists = await repository.findOne(predicate);
    if (ingredientAlreadyExists) throw new ExistingObjectException(predicate);

    return await repository.save(ingredient);
  }

  public async listAll(): Promise<Ingredient[]> {
    return await getMongoManager().getRepository(Ingredient).find();
  }

  public async editIngredient(
    ingredient: Ingredient,
    id: ObjectID
  ): Promise<Ingredient> {
    await Ingredient.validate(ingredient);

    const ingredientCustomRepository = getCustomRepository(
      IngredientRepository
    );
    await ingredientCustomRepository.getIngredientById(id);

    const ingredientRepository = getMongoManager().getRepository(Ingredient);

    await ingredientRepository.update(id, ingredient);

    return await ingredientRepository.findOne(id);
  }

  public async deleteIngredient(id: ObjectID): Promise<Ingredient> {
    const ingredientRepository = getCustomRepository(IngredientRepository);
    const ingredientFound = await ingredientRepository.getIngredientById(id);

    if (ingredientFound) {
      await getMongoManager().getRepository(Ingredient).delete(id);
    }
    return ingredientFound;
  }
}

export default new IngredientService();
